from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm
# Create your views here.

def todo_list_list(request):
    todo_list_list = TodoList.objects.all()
    context = {
        "todo_list_list": todo_list_list,
    }
    return render(request, "todos/todolist.html", context)

def todo_list_detail(request, id):
    todo_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_detail": todo_detail,
    }

    return render(request, "todos/tododetail.html", context )

def create_todolist(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()

    context = {
        "form":form
    }

    return render(request, "todos/todocreate.html", context)

def edit_todolist(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=todolist)

    context = {
        "TodoList_form": form,
        "TodoList_object": todolist,
    }

    return render(request, "todos/todoedit.html", context)

def delete_TodoList(request, id):
  model_instance = TodoList.objects.get(id=id)
  if request.method == "POST":
    model_instance.delete()
    return redirect("todo_list_list",)

  return render(request, "todos/tododelete.html")

def create_todoitem(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            New_task = form.save()
            return redirect("todo_list_detail", id=New_task.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form":form
    }

    return render(request, "todos/todonewitem.html", context)


def edit_todoitem(request, id):
    todoitem = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitem)
        if form.is_valid():
            Edited_task = form.save()
            return redirect("todo_list_detail", id=Edited_task.list.id)
    else:
        form = TodoItemForm(instance=todoitem)

    context = {
        "TodoItem_form": form,
        "TodoItem_object": todoitem,
    }

    return render(request, "todos/todoedititem.html", context)
